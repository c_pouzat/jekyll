---
layout: post
title:  "Pourquoi devrions nous arrêter d'embêter les gens avec la « recherche reproductible » et autres « bonnes pratiques » ?"
date:   2020-12-12
categories: covid
---


# Introduction

Le dialogue imaginaire qui suit est une conséquence (de plus) de la Covid 19. Depuis un peu plus de 15 ans, j'« embête » étudiants et collègues pour les convaincre du bien fondé d'une façon de faire et de présenter un travail scientifique : suivre les principes de la « recherche reproductible » &#x2013; je vais les énoncer dans les grandes lignes après cette introduction. Voilà qu'arrivent la Covid 19 et les modélisateurs d'épidémie. Cet événement constitue, à mes yeux, la démonstration la plus flagrante de l'inanité de la démarche que j'ai prônée. Ma conclusion découle du *report 9*<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> du groupe du Professeur Ferguson à l'Imperial College de Londres, du rôle qu'a joué ce rapport dans la décision britannique de confiner l'ensemble de la population, du rôle qu'il semble avoir joué dans la même décision chez nous <sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup>&#x2013; j'espère que nous en saurons plus sur ce point bientôt &#x2013; et de ce que nous apprennent les quelques examens maintenant disponibles du modèle et des simulations de ce rapport.


# Un dialogue (de sourds)

(Ce dialogue qui n'en est pas un au sens strict, peut aussi être vu comme une communication sur la recherche reproductible lors d'une « session flash<sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup> » d'un congrès scientitique, accompagnée / entrelacée des notes prises par un auditeur visiblement dubitatif.)

-   Chers collègues, chers étudiants, nous devrions toujours documenter les programmes que nous développons pour notre travail de recherche. C'est la meilleure garantie de pérennité de cette partie de notre travail. Cette documentation nous permettra de ré-examiner nos codes si des erreurs sont constatées, même plusieurs mois ou années après leur écriture. Cela nous permettra aussi de développer de nouveaux programmes, basés sur nos anciens et surtout, cela permettra à d'autres, de notre labo ou d'ailleurs (si nous rendons le code accessible) de faire de même.
-   Mais le Professeur Ferguson explique à propos du programme utilisé pour le rapport 9<sup><a id="fnr.4" class="footref" href="#fn.4">4</a></sup> : « J'ai écrit ce code (des milliers de lignes de code `C` *non documentées*<sup><a id="fnr.5" class="footref" href="#fn.5">5</a></sup>) il y a plus de 13 ans pour modéliser une pandémie de grippe&#x2026; »
-   Nous devrions aussi rendre nos programmes publics, nous sommes financés par des fonds publics et, comme la plupart de nos programmes sont peu utilisés, cela augmente les chances que les inévitables *bugs* soient trouvés. Rappelez vous qu'il a fallu 8 ans pour que Don Knuth déclare TeX <sup><a id="fnr.6" class="footref" href="#fn.6">6</a></sup>, son programme de composition de documents,  *bug free*. Or TeX était un programme ouvert, très utilisé par des gens qui savaient ce qu'ils faisaient et pour lequel les erreurs étaient très visibles.
-   Mais le Professeur Ferguson n'a pas rendu son programme public. Une version ré-écrite (par qui ?) a été rendue publique<sup><a id="fnr.7" class="footref" href="#fn.7">7</a></sup> en avril dernier, alors que ce « même » programme était utilisé depuis plus de 13 ans.
-   Nous devrions documenter nos données comme les paramètres utilisés par nos programmes, c'est la seule façon de pouvoir réutiliser, vérifier, partager cette partie de notre travail.
-   Mais je me répète, il n'y a pas traces de cela dans l'abondante production du Professeur Ferguson depuis son travail sur la « vache folle » au milieu des années 90. C'est en tout cas ce que suggère l'échange suivant sur le « problème 144 » du site GitHub<sup><a id="fnr.8" class="footref" href="#fn.8">8</a></sup> :
    -   (Wes Hinsley, un membre du labo de Ferguson) [&#x2026;] Plusieurs dizaines de milliers de simulations ont été utilisées pour modéliser la propagation de l'épidémie décrite dans le rapport 9.<sup><a id="fnr.9" class="footref" href="#fn.9">9</a></sup> [&#x2026;]
    -   (Franck Ch. Eigler) « Plusieurs dizaines de milliers de simulations&#x2026; » Y a-t-il une trace écrite [dans un fichier d'ordinateur] de celles-ci ? Si oui, quelles sont les raisons pour ne pas simplement les partager ? Si non&#x2026; ce serait très fâcheux.
    -   (Wes Hinsley) Seulement qu'il y a plusieurs dizaines de milliers de simulations. Comme je l'ai écrit, nous explorons des stratégies pour les partager d'une façon raisonnable.
-   Nous devrions rendre nos données, comme les paramètres utilisés par nos programmes, publics pour des raisons identiques à celles évoquées pour le partage des codes.
-   Cela m'inspire la même réplique que pour le dernier point.
-   Partager programmes, paramètres et données ne suffit pas, nous devons aussi expliquer, dans un « document reproductible »<sup><a id="fnr.10" class="footref" href="#fn.10">10</a></sup>, comment les programmes et les paramètres sont appliqués aux données pour obtenir les résultats (tables, figures) de nos articles ; puis partager ce « document ». Cela rend la détection et la correction des inévitables erreurs beaucoup plus efficaces ; cela permet à d'autres de critiquer notre travail et de construire sur celui-ci.
-   Pourquoi s'embêter ainsi, même le « rapport 10 »<sup><a id="fnr.11" class="footref" href="#fn.11">11</a></sup>, réplique du 9 avec la version publique du programme, ne satisfait pas à ces critères ?
-   Enfin, mais j'ai presque honte de vous rappeler des principes méthodologiques aussi élémentaires, lorsque notre travail fait intervenir des modèles intrinsèquement aléatoires (du fait d'emploi de méthodes de  Monte-Carlo<sup><a id="fnr.12" class="footref" href="#fn.12">12</a></sup> par exemple), nous devons toujours faire beaucoup (entre 500 et 1000) de simulations pour une collection de paramètres donnée, puis caractériser la distribution des quantités d'intérêts par la moyenne et l'écart type (voir plus, boîtes à moustaches, etc).
-   Mais je ne comprends pas, l'équipe du Professeur Ferguson n'a effectué qu'*une seule simulation* par jeu de paramètres ; ils l'expliquent eux-mêmes dans l'introduction au « rapport 10 »<sup><a id="fnr.11.100" class="footref" href="#fn.11">11</a></sup>, on le voit dans le troisième point de la réplication d'une partie des simulations du « rapport 9 » (avec la version publique du programme) par Stephen Eglen<sup><a id="fnr.13" class="footref" href="#fn.13">13</a></sup> et c'est discuté dans l'évaluation de Edling et ses collaborateurs<sup><a id="fnr.14" class="footref" href="#fn.14">14</a></sup>, membres du groupe *Rapid Assistance in Modelling the Pandemic*<sup><a id="fnr.15" class="footref" href="#fn.15">15</a></sup> de la Royal Society. Pourquoi alors devrais-je être aussi « tatillon » ?
-   &#x2026;
-   (L'auditeur pour lui même) Ce type est fou à lier ! pendant qu'il va perdre son temps à documenter, rendre public, simuler à outrance, moi je vais publier beaucoup plus de papiers, j'aurai plus de chances d'avoir mes réponses aux appels d'offre acceptées, j'aurai plus de chances d'avoir un poste, peut-être même qu'un jour, qui sait, je me retrouverais à siéger dans une commission chargée d'évaluer le travail de ce fêlé&#x2026;


# Notes de bas de page

<sup><a id="fn.1" href="#fnr.1">1</a></sup> <https://www.imperial.ac.uk/mrc-global-infectious-disease-analysis/covid-19/report-9-impact-of-npis-on-covid-19/>

<sup><a id="fn.2" href="#fnr.2">2</a></sup> Voir la page 2 de l'avis du Conseil Scientifique du 12 mars 2020 : <https://solidarites-sante.gouv.fr/IMG/pdf/avis_conseil_scientifique_12_mars_2020.pdf>

<sup><a id="fn.3" href="#fnr.3">3</a></sup> La version pour « adultes » de ce que les jeunes sont invités à faire avec « Ma thèse en 180 secondes ».

<sup><a id="fn.4" href="#fnr.4">4</a></sup> <https://twitter.com/neil_ferguson/status/1241835454707699713>

<sup><a id="fn.5" href="#fnr.5">5</a></sup> C'est moi qui souligne.

<sup><a id="fn.6" href="#fnr.6">6</a></sup> <https://fr.wikipedia.org/wiki/TeX>

<sup><a id="fn.7" href="#fnr.7">7</a></sup> <https://github.com/mrc-ide/covid-sim>

<sup><a id="fn.8" href="#fnr.8">8</a></sup> <https://github.com/mrc-ide/covid-sim/issues/144>

<sup><a id="fn.9" href="#fnr.9">9</a></sup> Le modèle de Ferguson comporte plus de 900 paramètres, la plupart d'entre eux sont définis sur un intervalle de R (l'ensemble des réels). Si nous avions affaire à 900 paramètres binaires (prenant deux valeurs possibles), il y aurait déja plus que 10^896 combinaisons à explorer.

<sup><a id="fn.10" href="#fnr.10">10</a></sup> <http://publications-sfds.fr/index.php/stat_soc/article/view/448/422>

<sup><a id="fn.11" href="#fnr.11">11</a></sup> <https://github.com/mrc-ide/covid-sim/tree/master/report9>

<sup><a id="fn.12" href="#fnr.12">12</a></sup> <https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Monte-Carlo>

<sup><a id="fn.13" href="#fnr.13">13</a></sup> <https://zenodo.org/record/3865491#.XuPW_y-ZPGI>

<sup><a id="fn.14" href="#fnr.14">14</a></sup> <https://www.researchsquare.com/article/rs-82122/v3>

<sup><a id="fn.15" href="#fnr.15">15</a></sup> <https://royalsociety.org/topics-policy/Health%20and%20wellbeing/ramp/>
