---
layout: post
title:  "L'activité et la culture scientifiques à présent et telles qu'elles pourraient être"
date:   2020-12-24
categories: 
    - recherche
    - société
---

#  Introduction

Le teste suivant est un chapitre du livre [L’Autre Voie pour l’humanité](https://editionsdelga.fr/produit/lautre-voie-pour-lhumanite/), édité par André Prone et publié en 2018 par les éditions [Delga](https://editionsdelga.fr/). Il est apparu une première fois sur la toile relayé par le blogue [NBH : pour un nouveau bloc historique](http://nbh-pour-un-nouveau-bloc-historique.over-blog.com/) de mon ami Antoine Manessis en décembre 2019. 
    
Je commence par préciser quelques éléments de mon parcours pour
permettre au lecteur de contextualiser cette contribution. Je suis
chercheur statutaire auprès du centre national de la recherche
scientifique (CNRS) depuis 17 ans. J'ai fait des études de physique,
effectué une thèse (en Allemagne) en biophysique / neurobiologie et suis
actuellement dans un laboratoire de mathématiques. En plus de mes quatre
ans de thèse en Allemagne, j'ai passé deux ans aux États-Unis pour un
séjour postdoctoral et une année en Angleterre. Mon parcours m'a fait
côtoyer différentes cultures scientifiques tant du point de vue de la
discipline (physique, biologie, mathématiques) que nationales (France,
Allemagne, Angleterre, États-Unis).
    
Je vais décrire brièvement ce qu'est « la vie » d'un
chercheur aujourd'hui dans un domaine scientifique qui garde quelques
traits artisanaux &#x2013; par opposition à des domaines comme la physique des
particules qui, par les grands instruments qu'ils requièrent, donnent un
aspect plus « industriel » (grosses équipes, division du travail très
poussée, etc.) au travail quotidien du chercheur. Mon message à ce
niveau sera simple : les contraintes imposées aux chercheurs
&#x2013; disparition des postes statutaires, financement sur contrats à court
terme, incitation à travailler en étroite collaboration avec des
entreprises, fort poids donné *à la quantité de publications* dans
l'évaluation des chercheurs &#x2013; nuisent à la qualité, qui peut être
entendue comme la fiabilité, du travail produit et *disqualifient
d'emblée* nombres de questions et voies d'investigations intéressantes.

Ma perspective pour un monde post-capitaliste découle des conséquences
de l'utilisation de notre productivité collective pour notre
émancipation. Une productivité sainement exploitée devrait nous fournir
*à tous* plus de « temps libre » et nous permettre d'explorer des voies
moins balisées. Le chercheur serait ainsi libéré d'une partie
significative des contraintes qui ont généré mes critiques précédentes,
tandis que le « citoyen ordinaire », ayant le temps, pourrait
s'approprier les résultats des travaux de recherche et contribuer
pleinement aux débats démocratiques à la confluence des problèmes de
société et de nos connaissances « scientifiques ». Je pense aux
questions de réchauffement climatique, de « sortie du nucléaire » ou de
l'organisation d'une société capable de traiter dignement une population
de plus en plus âgée.

#  L'activité de recherche anciennement « artisanale » aujourd'hui

Je travaille en neurophysiologie sur l'olfaction des insectes et plus
généralement les méthodes d'analyse de données neurobiologiques (allant
de l'insecte au patient parkinsonien). Un aspect primordial de ce
travail, pour moi, est qu'il est possible de raisonnablement maîtriser
une bonne partie du processus de recherche : quand je faisais encore des
expériences sur les insectes, j'avais construit moi-même mon poste
expérimental, j'utilisais un amplificateur à la conception duquel
j'avais contribué et j'analysais les données avec des logiciels que
j'avais moi-même développés, avant d'écrire mes articles. Je peux
continuer à travailler comme je l'ai toujours fait, car j'ai la chance
d'avoir un emploi permanent au sein d'une institution qui me laisse
encore une grande liberté. Mais cette situation, qui était la plus
courante lorsque j'ai commencé ma thèse (il y a un peu plus de vingt
ans), devient une exception. Les postes permanents sont en voie de
disparition et au lieu de cela, les chercheurs travaillent maintenant
majoritairement sur des contrats type CDD. Même quand ils ont des postes
permanents, comme les « privilégiés » du CNRS, les chercheurs n'ont plus
que leur salaire assuré, les coûts de l'activité expérimentale
proprement dite doivent être couverts par des contrats d'une durée
typique de deux à cinq ans. Il faut être conscient ici qu'assurer le
fonctionnement d'un laboratoire en biologie expérimentale (achat du
matériel, entretien d'une ménagerie, etc.) nécessite annuellement
plusieurs centaines de milliers d'Euros. L'activité de recherche est
ainsi devenue entièrement « pilotée » par des agences de financement
&#x2013; comme l'Agence National de la Recherche (ANR), dont le budget s'est
réduit comme peau de chagrin ; le conseil européen de la recherche (ERC,
il faut évidemment traduire en anglais, *European Research Council* pour
que l'acronyme fasse sens) &#x2013; et par l'agenda politique via les
« initiatives d'excellences » (IDEX) utilisées pour imposer la fusion
des universités à marche forcée. La quasi-totalité des appels d'offres
auxquels les chercheurs sont invités à répondre encourage fortement les
partenariats publics / privés (moyen par lequel le contribuable en
arrivera bientôt à financer, dans sa totalité, l'activité recherche et
développement des entreprises).

Le résultat de cette évolution pour mon domaine (anciennement)
« artisanal » est une forte « rationalisation » du travail : le
directeur de labo ne fait plus d'expériences lui-même, au lieu de cela
il rédige des articles basés sur des données recueillies par les non
statutaires de son labo (stagiaires postdoctoraux et thésards) ; il
élabore la recherche, mais celle-ci est mise en œuvre avec une
instrumentation qu'il ne maîtrise plus puisque que cela fait des années
qu'il n'est plus « à la paillasse » ; il perd par là un regard critique
indispensable sur les données générées par son laboratoire &#x2013; ce qui lui
permettra, si besoin, de se délester de toute responsabilité s'il
s'avère qu'un article de son laboratoire, qu'il a signé, était basé sur
des données fausses ou arrangées ; le cas de David Baltimore est
emblématique à cet égard. Les non statutaires du labo « n'ont plus le
temps » de fabriquer leurs instruments, au lieu de cela ils rédigent des
parties de réponses à des appels d'offres qui leur permettront d'acheter
les instruments « tout faits ». Si nous nous souvenons du proverbe,
« J'entends, j'oublie ; je vois, je me souviens ; je fais, je
comprends » (attribué à Confucius ou Xun Zi suivant les sources) faire
un exercice de rhétorique pour pouvoir s'acheter un instrument, plutôt
que de le construire soi-même, implique que les expérimentateurs
modernes ont une compréhension de plus en plus superficielle de leurs
appareils ; voie royale pour la production d'artefacts&#x2026; Enfin, la
nécessité bien intégrée par tous de publier en quantité &#x2013; le directeur
de labo doit avoir son prochain financement ; la prolongation du contrat
du stagiaire postdoctoral dépend de ce financement et son hypothétique
futur CDI dépend de la longueur de sa liste de publications ; le thésard
ne pourra soutenir que s'il a assez d'articles &#x2013;, favorise la
« simplification » des articles, la sélection des données, etc. D'où ma
préoccupation avec la fiabilité du travail produit. Enfin toute
investigation potentiellement longue devient clairement trop « risquée »
pour être entreprise.

#  Perspectives pour un monde post-capitaliste

Ma perspective s'articule autour de deux éléments qui découlent tous les
deux d'une constatation simple et déjà ancienne : dans une société comme
la nôtre, nous devrions pouvoir utiliser notre productivité collective à
réellement émanciper (tous) les gens &#x2013; au lieu remplir les poches d'une
minorité exploiteuse. L'émancipation rendue possible par la productivité
signifie qu'un partage massif du travail est possible concomitamment à
un développement considérable du « temps libre ». Ce gain de temps libre
m'amène à l'élément « culturel » de mon titre. Les résultats
scientifiques sont aujourd'hui « confisqués » par leurs producteurs et,
plus encore, par leurs commanditaires publics (servant de plus en plus
souvent de faux nez aux intérêts privés) ou directement privés. Cette
situation constitue un obstacle considérable à l'établissement d'un
débat démocratique contradictoire sur des questions éminemment
politiques dans lesquelles l'aspect scientifique est prépondérant.
Rendre la population capable de débattre de façon constructive sur de
telles questions suppose la possibilité pour celle-ci de s'informer et
s'éduquer *tout au long de la vie*. Or s'éduquer et s'informer demande
du temps ; en dernière analyse une société « complexe » ou
« différenciée » ne peut être réellement démocratique, me semble-t-il,
que si ses citoyens disposent d'un temps considérable pour s'éduquer et
s'informer *librement*.

Le second élément est constitué par l'activité de recherche elle-même ;
une pleine exploitation de la productivité au service de la société
devrait permettre d'émanciper la recherche de la perspective
« court-termiste valorisable directement sur le marché » dans laquelle
elle est actuellement engagée (je caricature à peine). L'éducation
permanente rendue possible par une augmentation du temps libre devrait
rendre l'activité de recherche accessible à plus de personnes,
désenclavant par là certains domaines « bloqués » par une trop grande
uniformité (intellectuelle) de leurs contributeurs. Enfin, un abandon du
« productivisme » actuel en science devrait permettre aux scientifiques
de participer systématiquement à l'éducation / information de leurs
concitoyens, contribuant par là à créer un véritable « cercle
vertueux ».

